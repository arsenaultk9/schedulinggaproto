﻿using System;
using AForge.Genetic;
using SchedulingGAProto.Genetic;
using SchedulingGAProto.PlantData;
using SchedulingGAProto.Scheduling;

namespace SchedulingGAProto
{
    class Program
    {
        const int PopulationPrintRate = 100;
        const int IterationCount = 1000;

        static void Main(string[] args)
        {
            var schedulingStartDate = new DateTime(2017, 1, 31);
            var schedulingCosts = new SchedulingCosts(latenessCostRate: 2, setupCostRate: 1);

            var products = ProductsFactory.GetProducts();
            var tasks = TasksFactory.GetTasks(products);

            ISelectionMethod selectionMethod = new EliteSelection();

            var schedulePopulation = PopulationFactory.GetPopulation(schedulingStartDate, schedulingCosts, tasks, products, selectionMethod);

            using (var printer = new PopulationPrinter("results.txt"))
            {
                printer.PrintPopulation(schedulePopulation, 0);

                var trainingIndex = 1;
                while (trainingIndex <= IterationCount)
                {
                    schedulePopulation.RunEpoch();

                    if (trainingIndex % PopulationPrintRate == 0)
                    {
                        printer.PrintPopulation(schedulePopulation, trainingIndex, trainingIndex == IterationCount);
                    }

                    trainingIndex++;
                }
            }

            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
    }
}
