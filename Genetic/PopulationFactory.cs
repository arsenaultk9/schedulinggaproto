﻿using System;
using System.Collections.Generic;
using AForge.Genetic;
using SchedulingGAProto.PlantData;
using SchedulingGAProto.Scheduling;

namespace SchedulingGAProto.Genetic
{
    public static class PopulationFactory
    {
        private const int PopulationSize = 100;

        public static Population GetPopulation(DateTime schedulingStartDate, SchedulingCosts schedulingCosts, List<Task> tasks, List<Product> products, ISelectionMethod selectionMethod)
        {
            var setupTimeCalculator = SetupTimeCalculatorFactory.GetSetupTimeCalculator(products);
            var scheduler = new Scheduler(schedulingStartDate, schedulingCosts, setupTimeCalculator);

            var fitnessFunction = new SchedulingFitnessFunction(scheduler);

            return new Population(PopulationSize, new ScheduleChromosome(tasks), fitnessFunction, selectionMethod)
            {
                MutationRate = 0.1,
                CrossoverRate = 0.1
            };
        }
    }
}
