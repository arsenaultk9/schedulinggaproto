﻿using AForge.Genetic;
using SchedulingGAProto.Scheduling;

namespace SchedulingGAProto.Genetic
{
    class SchedulingFitnessFunction : IFitnessFunction
    {
        private readonly Scheduler scheduler;
        public SchedulingFitnessFunction(Scheduler scheduler)
        {
            this.scheduler = scheduler;
        }

        public double Evaluate(IChromosome chromosome)
        {
            var scheduleChromosome = (ScheduleChromosome) chromosome;
            var schedule = scheduler.ScheduleTasks(scheduleChromosome.Tasks);
            scheduleChromosome.Cost = schedule.TotalCosts;

            return 1/schedule.TotalCosts;
        }
    }
}
