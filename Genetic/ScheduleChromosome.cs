﻿using System.Collections.Generic;
using System.Linq;
using AForge.Genetic;
using SchedulingGAProto.PlantData;

namespace SchedulingGAProto.Genetic
{
    class ScheduleChromosome : ChromosomeBase
    {
        public List<Task> Tasks;
        private readonly int taskCount;

        public double Cost;
        public ScheduleChromosome(List<Task> tasks)
        {
            this.taskCount = tasks.Count;
            this.Tasks = tasks.ToList();

            Generate();
        }

        public ScheduleChromosome(ScheduleChromosome source)
        {
            this.Tasks = source.Tasks.ToList();
            this.taskCount = source.taskCount;

            this.fitness = source.fitness;
            this.Cost = source.Cost;
        }

        public override string ToString()
        {
            var taskNames = new string[taskCount];

            for (var taskIndex = 0; taskIndex < taskCount; taskIndex++)
            {
                taskNames[taskIndex] = Tasks[taskIndex].Number.ToString();
            }

            return string.Join(", ", taskNames);
        }

        public sealed override void Generate()
        {
            Tasks.Shuffle();
        }

        public override IChromosome CreateNew()
        {
            return new ScheduleChromosome(Tasks);
        }

        public override IChromosome Clone()
        {
            return new ScheduleChromosome(this);
        }

        public override void Mutate()
        {
            var itemAIndex = ThreadSafeRandom.ThisThreadsRandom.Next(0, taskCount - 1);
            var itemBIndex = ThreadSafeRandom.ThisThreadsRandom.Next(0, taskCount - 1);

            var temp = Tasks[itemAIndex];

            Tasks[itemAIndex] = Tasks[itemBIndex];
            Tasks[itemBIndex] = temp;
        }

        public override void Crossover(IChromosome pair)
        {
            var pairChromosome = (ScheduleChromosome)pair;
            var pairChromosomeToInspireForIndex = ThreadSafeRandom.ThisThreadsRandom.Next(0, taskCount - 1);
            var pairChromosomeToInspireFor = pairChromosome.Tasks[pairChromosomeToInspireForIndex];

            var itemInSourceIndex = 0;
            while (Tasks[itemInSourceIndex].Number != pairChromosomeToInspireFor.Number)
            {
                itemInSourceIndex++;
            }

            var temp = Tasks[pairChromosomeToInspireForIndex];

            Tasks[pairChromosomeToInspireForIndex] = Tasks[itemInSourceIndex];
            Tasks[itemInSourceIndex] = temp;

        }
    }
}
