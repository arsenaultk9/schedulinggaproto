﻿using System;
using System.Collections.Generic;
using SchedulingGAProto.PlantData;

namespace SchedulingGAProto.Scheduling
{
    public class SetupTimeCalculator
    {
        private readonly List<ProductChangeSetup> productChangeSetups;

        public SetupTimeCalculator(List<ProductChangeSetup> productChangeSetups)
        {
            this.productChangeSetups = productChangeSetups;
        }

        public TimeSpan GetTaskSetup(Task fromTask, Task toTask)
        {
            if(fromTask == null) return TimeSpan.Zero;

            foreach (var productChangeSetup in productChangeSetups)
            {
                var setupTime = productChangeSetup.GetSetupTime(fromTask, toTask);
                if(setupTime > TimeSpan.Zero) return setupTime;
            }

            return TimeSpan.Zero;
        }
    }
}
