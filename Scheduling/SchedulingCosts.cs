﻿namespace SchedulingGAProto.Scheduling
{
    public class SchedulingCosts
    {
        public readonly int LatenessCostRate;
        public readonly int SetupCostRate;

        public SchedulingCosts(int latenessCostRate, int setupCostRate)
        {
            LatenessCostRate = latenessCostRate;
            SetupCostRate = setupCostRate;
        }
    }
}
