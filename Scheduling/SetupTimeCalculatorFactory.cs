﻿using System;
using System.Collections.Generic;
using System.Linq;
using SchedulingGAProto.PlantData;

namespace SchedulingGAProto.Scheduling
{
    public static class SetupTimeCalculatorFactory
    {
        public static SetupTimeCalculator GetSetupTimeCalculator(List<Product> products)
        {
            return new SetupTimeCalculator(new []
            {
                new ProductChangeSetup(products[0], products[1], TimeSpan.FromHours(2)),
                new ProductChangeSetup(products[0], products[2], TimeSpan.FromHours(6)),
                new ProductChangeSetup(products[1], products[0], TimeSpan.FromHours(2)),
                new ProductChangeSetup(products[1], products[2], TimeSpan.FromHours(6)),
                new ProductChangeSetup(products[2], products[0], TimeSpan.FromHours(2)),
                new ProductChangeSetup(products[2], products[1], TimeSpan.FromHours(4))
            }.ToList());
        }
    }
}
