﻿using System;
using System.Collections.Generic;
using SchedulingGAProto.PlantData;

namespace SchedulingGAProto.Scheduling
{
    public class Scheduler
    {
        private readonly DateTime schedulingStartDate;
        private readonly SchedulingCosts schedulingCosts;
        private readonly SetupTimeCalculator setupTimeCalculator;


        public Scheduler(DateTime schedulingStartDate, SchedulingCosts schedulingCosts, SetupTimeCalculator setupTimeCalculator)
        {
            this.setupTimeCalculator = setupTimeCalculator;
            this.schedulingCosts = schedulingCosts;
            this.schedulingStartDate = schedulingStartDate;
        }

        public Scheduling.Schedule ScheduleTasks(List<Task> tasks)
        {
            var scheduledTasks = new List<PlannedTask>();
            var latnessCosts = 0.0;
            var setupCosts = 0.0;

            var currentTimeInSchedule = schedulingStartDate;
            Task previousTask = null;

            foreach (var task in tasks)
            {
                var plannedTask = new PlannedTask(task, currentTimeInSchedule);
                scheduledTasks.Add(plannedTask);

                latnessCosts += plannedTask.Lateness.TotalHours *schedulingCosts.LatenessCostRate;
                setupCosts += setupTimeCalculator.GetTaskSetup(previousTask, task).TotalHours * schedulingCosts.LatenessCostRate;

                currentTimeInSchedule = plannedTask.EndDate;
                previousTask = task;
            }

            return new Scheduling.Schedule(scheduledTasks, latnessCosts, setupCosts);
        }
    }
}
