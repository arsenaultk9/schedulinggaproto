﻿using System;
using SchedulingGAProto.PlantData;

namespace SchedulingGAProto.Scheduling
{
    public class PlannedTask
    {
        public readonly Task Task;
        public readonly DateTime StartDate;

        public DateTime EndDate => StartDate + Task.Lenght;

        public TimeSpan Lateness => EndDate > Task.RequiredDate
            ? EndDate - Task.RequiredDate
            : TimeSpan.Zero;


        public PlannedTask(Task task, DateTime startDate)
        {
            this.Task = task;
            StartDate = startDate;
        }

        public override string ToString()
        {
            return $"{Task.Number}: Start({StartDate}), End({EndDate}), Lateness({Lateness})";
        }
    }
}
