﻿using System.Collections.Generic;

namespace SchedulingGAProto.Scheduling
{
    public class Schedule
    {
        public readonly List<PlannedTask> PlannedTasks;

        public readonly double LatenessCost;
        public readonly double SetupTimeCost;
        public double TotalCosts => LatenessCost + SetupTimeCost;

        public Schedule(List<PlannedTask> plannedTasks, double latenessCost, double setupTimeCost)
        {
            PlannedTasks = plannedTasks;
            LatenessCost = latenessCost;
            SetupTimeCost = setupTimeCost;
        }
    }
}
