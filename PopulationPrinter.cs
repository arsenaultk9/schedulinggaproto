﻿using System;
using System.IO;
using AForge.Genetic;
using SchedulingGAProto.Genetic;

namespace SchedulingGAProto
{
    public class PopulationPrinter : IDisposable
    {
        private readonly StreamWriter fileWritter;

        public PopulationPrinter(string fileName)
        {
            fileWritter = new StreamWriter(fileName);
        }

        public void PrintPopulation(Population population, int trainingCycleIndex, bool isLastResult = false)
        {
            var bestChromosome = (ScheduleChromosome)(population.BestChromosome ?? population[0]);

            Console.WriteLine("==================================================");
            Console.WriteLine("{0}) Best chromosome: {1} || fitness: {2} || Cost: {3}", trainingCycleIndex, bestChromosome, bestChromosome.Fitness, bestChromosome.Cost);
            Console.WriteLine("==================================================");

            fileWritter.WriteLine("==================================================");
            fileWritter.WriteLine("{0}) Best chromosome: {1} || fitness: {2} || Cost: {3}", trainingCycleIndex, bestChromosome, bestChromosome.Fitness, bestChromosome.Cost);
            fileWritter.WriteLine("==================================================");

            if (!isLastResult) return;

            using (var csvResults = new StreamWriter("results.csv"))
            {
                csvResults.WriteLine(bestChromosome);
            }

            fileWritter.WriteLine("Other chromosomes in population: ");

            for (var populationIndex = 0; populationIndex < population.Size; populationIndex++)
            {
                var chromosome = (ScheduleChromosome)population[populationIndex];
                fileWritter.WriteLine("{0}) chromosome: {1} || fitness: {2} || Cost: {3}", populationIndex, chromosome, chromosome.Fitness, chromosome.Cost);
            }
        }

        public void Dispose()
        {
            fileWritter.Dispose();
        }
    }
}
