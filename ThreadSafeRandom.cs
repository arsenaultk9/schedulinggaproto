﻿using System;
using System.Threading;

namespace SchedulingGAProto
{
    public static class ThreadSafeRandom
    {
        [ThreadStatic]
        private static Random local;

        public static Random ThisThreadsRandom => local ?? (local = new Random(unchecked(Environment.TickCount * 31 + Thread.CurrentThread.ManagedThreadId)));
    }
}
