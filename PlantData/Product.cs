﻿namespace SchedulingGAProto.PlantData
{
    public class Product
    {
        public readonly string Name;

        public Product(string name)
        {
            Name = name;
        }
    }
}
