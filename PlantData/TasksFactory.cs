﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SchedulingGAProto.PlantData
{
    public static class TasksFactory
    {
        public static List<Task> GetTasks(List<Product> products)
        {
            return new[]
            {
                new Task(1, TimeSpan.FromHours(18), new DateTime(2017, 2, 1), products[2]),
                new Task(2, TimeSpan.FromHours(12), new DateTime(2017, 2, 1), products[0]),
                new Task(3, TimeSpan.FromHours(5), new DateTime(2017, 2, 1), products[1]),
                new Task(4, TimeSpan.FromHours(13), new DateTime(2017, 2, 2), products[0]),
                new Task(5, TimeSpan.FromHours(22), new DateTime(2017, 2, 3), products[0]),
                new Task(6, TimeSpan.FromHours(4), new DateTime(2017, 2, 5), products[2]),
                new Task(7, TimeSpan.FromHours(11), new DateTime(2017, 2, 5), products[0]),
                new Task(8, TimeSpan.FromHours(18), new DateTime(2017, 2, 5), products[2]),
                new Task(9, TimeSpan.FromHours(8), new DateTime(2017, 2, 7), products[2]),
                new Task(10, TimeSpan.FromHours(12), new DateTime(2017, 2, 7), products[1]),
                new Task(11, TimeSpan.FromHours(13), new DateTime(2017, 2, 8), products[2]),
                new Task(12, TimeSpan.FromHours(19), new DateTime(2017, 2, 10), products[0]),
                new Task(13, TimeSpan.FromHours(9), new DateTime(2017, 2, 10), products[0]),
                new Task(14, TimeSpan.FromHours(5), new DateTime(2017, 2, 10), products[1]),
                new Task(15, TimeSpan.FromHours(6), new DateTime(2017, 2, 10), products[1]),
                new Task(16, TimeSpan.FromHours(8), new DateTime(2017, 2, 10), products[2]),
                new Task(17, TimeSpan.FromHours(12), new DateTime(2017, 2, 11), products[0]),
                new Task(18, TimeSpan.FromHours(13), new DateTime(2017, 2, 14), products[1]),
                new Task(19, TimeSpan.FromHours(11), new DateTime(2017, 2, 15), products[0]),
                new Task(20, TimeSpan.FromHours(21), new DateTime(2017, 2, 15), products[0])
            }.ToList();
        }
    }
}
