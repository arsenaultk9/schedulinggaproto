﻿using System;

namespace SchedulingGAProto.PlantData
{
    public class ProductChangeSetup
    {
        private readonly Product fromProduct;
        private readonly Product toProduct;
        private readonly TimeSpan setupTime;

        public ProductChangeSetup(Product fromProduct, Product toProduct, TimeSpan setupTime)
        {
            this.fromProduct = fromProduct;
            this.toProduct = toProduct;
            this.setupTime = setupTime;
        }

        public TimeSpan GetSetupTime(Task fromTask, Task toTask)
        {
            if (fromTask.Product == fromProduct && toTask.Product == toProduct)
            {
                return setupTime;
            }

            return TimeSpan.Zero;
        }
    }
}
