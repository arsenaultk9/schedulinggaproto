﻿using System;

namespace SchedulingGAProto.PlantData
{
    public class Task
    {
        public readonly int Number;
        public readonly TimeSpan Lenght;
        public readonly DateTime RequiredDate;
        public readonly Product Product;

        public Task(int number, TimeSpan lenght, DateTime requiredDate, Product product)
        {
            Number = number;
            Lenght = lenght;
            RequiredDate = requiredDate;
            Product = product;
        }

        public override string ToString()
        {
            return Number.ToString();
        }
    }
}
