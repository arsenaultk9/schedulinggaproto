﻿using System.Collections.Generic;
using System.Linq;

namespace SchedulingGAProto.PlantData
{
    public static class ProductsFactory
    {
        public static List<Product> GetProducts()
        {
            return new[]
            {
                new Product("P1"),
                new Product("P2"),
                new Product("P3")
            }.ToList();
        }
    }
}
